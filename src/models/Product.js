const mongoose = require('mongoose')
// const mongoosePaginate = require('mongoose-paginate')
const mongoosePaginate = require('mongoose-paginate-v2')

const ProductSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true,
    },
    createdAt: { //data de criação do produto
        type: Date,
        default: Date.now,
    },
})

ProductSchema.plugin(mongoosePaginate)

// mongoose.model('Product', ProductSchema); //registra o model para toda a aplicação

const model = mongoose.model('Product', ProductSchema); //v2

model.paginate().then({})
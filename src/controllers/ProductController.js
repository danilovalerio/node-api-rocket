//tratar as operações de determinado model
const mongoose = require('mongoose');

const Product = mongoose.model('Product')

module.exports = {
    async index(req, res){
        const { page = 1 } = req.query //query para pagarâmetros get
        const products = await Product.paginate({},{ page, limit: 10}); //espera a busca no banco e executa a próxima linha

        return res.json(products); //json -javascript object notation
    },

    async show(req, res){
        const product = await Product.findById(req.params.id)

        return res.json(product)
    },

    async store(req, res){
        //Criação
        const product = await Product.create(req.body)

        return res.json(product)
    },

    async update(req, res){
        const product = await Product.useFindAndModify(req.params.id, req.body, {new: true})
        return res.json(product)
    },

    async destroy(req, res){
        await Product.findByIdAndRemove(req.params.id)
        
        return res.send()
    },
}
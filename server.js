//configuração do servidor
const express = require('express')
const mongoose = require('mongoose')
const requireDir = require('require-dir')

const app = express();

//permite que é possível enviar informações no formato json
app.use(express.json())

//Start o DB, conexão com o banco
mongoose.connect('mongodb://127.0.0.1:27017/nodeapirocket', { useNewUrlParser: true})

requireDir('./src/models') //registro da model no projeto

// const Product = mongoose.model('Product')

//a partir da rota api
app.use("/api", require("./src/routes"))

app.listen(3001)

